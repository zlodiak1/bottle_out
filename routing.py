from bottle import template, route
import psycopg2
import config 


@route('/')
@route('/page1')
def page1():
    names = []
    try:
        connection = psycopg2.connect(
            dbname=config.dbname, 
            user=config.user, 
            password=config.password, 
            host=config.host
        )
    except Exception as e:
        print('error db connection')

    try:
        cursor = connection.cursor()
        query = 'select * from names;'
        cursor.execute(query)
        records = cursor.fetchall() 
        for row in records:
            names.append(row[1])
    except:
        print('error table data')

    return template('templates/page1', names=names)


@route('/page2')
def page2():
    return template('templates/page2')
